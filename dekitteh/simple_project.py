import math

def f(x:float, y:float) -> float:
    return (2 * x * (x**2 + y))

def exact(x:float) -> float:
    return (math.e**(x**2) - x**2 - 1)

defaults = {
    'x0': 0,
    'y0': 0,
    'xn': 10,
    'n' : 5,
    'n0': 3,
    'N' : 100,
    'euler':          '#ff0000',
    'improved_euler': '#00ff00',
    'runge_kutta':    '#0000ff'
}
 
