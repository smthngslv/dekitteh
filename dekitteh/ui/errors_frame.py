import tkinter
import tkinter.ttk

from threading import Thread

import dekitteh.solutions as solutions

from dekitteh.ui.widgets import Canvas

class ErrorsFrame(tkinter.ttk.Frame):
    """
    The class, that compute dependance of errors on x for the second tab.
    
    Methods
    -------
    
    rebuild(f:callable, e:callable, x0:float, y0:float, xn:float, n0:int, N:int **kwargs) -> None
        Build all graphs.
    
    """
    
    def __init__(self, window:tkinter.Misc, **kwargs):
        """
        Parameters
        ----------
        
        window : tkinter.Misc
            A tkinter parent element.
            
        kwargs : dict
            The same as tkinter.ttk.Frame.
       
        """
        
        super().__init__(window, **kwargs)
        
        self.__canvas = Canvas(self)

        self.__canvas.grid(row = 0, column = 0, sticky = tkinter.E + tkinter.S + tkinter.W + tkinter.N)
        
        # Make a rezable window.
        self.rowconfigure(0, weight = 1)
        self.columnconfigure(0, weight = 1)
        
        self.__background_task = None
        
    def rebuild(self, f:callable, e:callable, x0:float, y0:float, xn:float, n0:int, N:int, **kwargs) -> None:
        """
        Build all graphs.
        
        Parameters
        ----------
        
        f : callable
            A function f(x, y).
            
        e : callable
            A function exact(x).
        
        x0, y0 : float
            Initial values.
            
        xn : float
            Last point to approximate.
            
        n0 : int
            The min number of steps.
            
        N : int
            Max number of steps.
            
            
        kwargs
        ------
            
        euler : dict
            Kwargs for the euler graph for the Canvas.plot_graph method.
            
        improved_euler : dict
            Kwargs for the improved euler graph for the Canvas.plot_graph method.
            
        runge kutta : dict
            Kwargs for the runge kutta graph for the Canvas.plot_graph method.
        
        """
        
        # TODO: make smth cool.
        if self.__background_task:
            self.__background_task._stop()
            self.__background_task.join()
            
        self.__background_task = Thread(target = self.__compute, args = [f, e, x0, y0, xn, n0, N], kwargs = kwargs)
        self.__background_task.start()
        
    def __compute(self, f:callable, e:callable, x0:float, y0:float, xn:float, n0:int, N:int, **kwargs):
        euler          = []
        improved_euler = []
        runge_kutta    = []
        
        for n_i in range(n0, N + 1):
            euler.append((n_i, solutions.Euler.solve(x0, y0, xn, n_i, f, e)[1][-1][1]))
            
            improved_euler.append((n_i, solutions.ImprovedEuler.solve(x0, y0, xn, n_i, f, e)[1][-1][1]))
            
            runge_kutta.append((n_i, solutions.RungeKutta.solve(x0, y0, xn, n_i, f, e)[1][-1][1]))
            
        self.__canvas.delete('graph')
        self.__canvas.plot_graph('euler',          euler,          **kwargs.get('euler', {}))
        self.__canvas.plot_graph('improved_euler', improved_euler, **kwargs.get('improved_euler', {}))
        self.__canvas.plot_graph('runge_kutta',    runge_kutta,    **kwargs.get('runge_kutta', {}))
