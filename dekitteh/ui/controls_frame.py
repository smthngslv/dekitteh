import logging
import tkinter
import tkinter.ttk
import tkinter.colorchooser

from dekitteh.ui.widgets import GraphColor, ProjectLoader, NumericVariable

class ControlsFrame(tkinter.ttk.Frame):
    """
    Represents all controls.
    
    Attributes
    ----------
        
    project : ProjectLoader
        A widget to load the project.
        
    euler, improved_euler, runge_kutta, exact: GraphColor
        A widgets to change the color of corresponding graphs.
        
    x0, y0, xn, n, n0, N : NumericVariable
        A widgets to change corresponding values.
     
    """
    
    def __init__(self, window:tkinter.Misc, **kwargs):
        """
        Parameters
        ----------
        
        window : tkinter.Misc
            A tkinter parent element.
            
        kwargs : dict
            The same as tkinter.ttk.Frame.
       
        """
        
        super().__init__(window, **kwargs)
        
        self.project = ProjectLoader(self)
        
        self.euler          = GraphColor(self, 'Euler:')
        self.improved_euler = GraphColor(self, 'Improved Euler:')
        self.runge_kutta    = GraphColor(self, 'Runge-Kutta:')
        self.exact          = GraphColor(self, 'Exact:')
        
        self.x0 = NumericVariable(self, 'x_0 = ')
        self.y0 = NumericVariable(self, 'y_0 = ')
        self.xn = NumericVariable(self, 'X = ')
        self.n  = NumericVariable(self, 'N = ',     accuracy = 0)
        self.n0 = NumericVariable(self, 'N_min = ', accuracy = 0)
        self.N  = NumericVariable(self, 'N_max = ', accuracy = 0)
        
        
        self.project.grid(row = 0, column = 0, sticky = tkinter.E + tkinter.W)
        
        self.euler.grid(         row = 2, column = 0, sticky = tkinter.E + tkinter.W)
        self.improved_euler.grid(row = 4, column = 0, sticky = tkinter.E + tkinter.W)
        self.runge_kutta.grid(   row = 6, column = 0, sticky = tkinter.E + tkinter.W)
        self.exact.grid(         row = 8, column = 0, sticky = tkinter.E + tkinter.W)
        
        
        self.x0.grid(row = 10,  column = 0, sticky = tkinter.E + tkinter.W)
        self.y0.grid(row = 12, column = 0, sticky = tkinter.E + tkinter.W)
        self.xn.grid(row = 14, column = 0, sticky = tkinter.E + tkinter.W)
        self.n.grid( row = 16, column = 0, sticky = tkinter.E + tkinter.W)
        self.n0.grid(row = 18, column = 0, sticky = tkinter.E + tkinter.W)
        self.N.grid( row = 20, column = 0, sticky = tkinter.E + tkinter.W)
        
        
        # Make a rezable window.
        for i in range(0, 22, 2):
            self.rowconfigure(i,  weight = 1)
            
        # Just for look fine.
        for i in range(1, 21, 2):
            tkinter.ttk.Separator(self, orient = tkinter.HORIZONTAL).grid(row = i, column = 0, sticky = tkinter.E + tkinter.W)

        self.project.bind('<<ProjectLoader>>', self.__on_open)

    def __on_open(self, event:tkinter.Event) -> None: 
        try:
            for var in self.project.defaults:
                setattr(getattr(self, var), 'value', self.project.defaults[var])
            
        except Exception as exc:
            self.project.close()
        
            logging.warning('First computing.', exc_info = True)
            
