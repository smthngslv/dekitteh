import tkinter
import tkinter.ttk

import dekitteh.solutions as solutions

from dekitteh.ui.widgets import Canvas

class ApproximationFrame(tkinter.ttk.Frame):
    """
    The class, that compute aproximations for the first tab.
    
    Methods
    -------
    
    rebuild(f:callable, e:callable, x0:float, y0:float, xn:float, n:int **kwargs) -> None
        Build all graphs.
    
    """
    
    def __init__(self, window:tkinter.Misc, **kwargs):
        """
        Parameters
        ----------
        
        window : tkinter.Misc
            A tkinter parent element.
            
        kwargs : dict
            The same as tkinter.ttk.Frame.
       
        """
        
        super().__init__(window, **kwargs)
        
        # Divide the into two parts.
        self.__graphs_panel = tkinter.ttk.PanedWindow(self, orient = tkinter.VERTICAL)
        
        self.__label0 = tkinter.ttk.Label(self, text = 'Solutions', anchor = tkinter.CENTER)
        self.__label1 = tkinter.ttk.Label(self, text = 'GTE',       anchor = tkinter.CENTER)
        
        # Graphs.
        self.__canvas_errors        = Canvas(self.__graphs_panel)
        self.__canvas_approximation = Canvas(self.__graphs_panel)
        
        self.__graphs_panel.add(self.__canvas_approximation, weight = 1)
        self.__graphs_panel.add(self.__canvas_errors,        weight = 1)
        
        self.__graphs_panel.grid(row = 1, column = 0, sticky = tkinter.E + tkinter.S + tkinter.W + tkinter.N)
        self.__label0.grid(row = 0, column = 0, sticky = tkinter.E + tkinter.S + tkinter.W + tkinter.N)
        self.__label1.grid(row = 2, column = 0, sticky = tkinter.E + tkinter.S + tkinter.W + tkinter.N)
        
        # Make a rezable window.
        self.rowconfigure(1, weight = 1)
        self.columnconfigure(0, weight = 1)
        
    def rebuild(self, f:callable, e:callable, x0:float, y0:float, xn:float, n:int, **kwargs) -> None:
        """
        Build all graphs.
        
        Parameters
        ----------
        
        f : callable
            A function f(x, y).
            
        e : callable
            A function exact(x).
        
        x0, y0 : float
            Initial values.
            
        xn : float
            Last point to approximate.
            
        n : int
            Target number of steps.
             
        kwargs
        ------
            
        euler : dict
            Kwargs for the euler graph for the Canvas.plot_graph method.
            
        improved_euler : dict
            Kwargs for the improved euler graph for the Canvas.plot_graph method.
            
        runge kutta : dict
            Kwargs for the runge kutta graph for the Canvas.plot_graph method.
        
        exact : dict
            Kwargs for the exact graph for the Canvas.plot_graph method.
        
        """
        
        euler          = solutions.Euler.solve(x0, y0, xn, n, f, e)
        
        improved_euler = solutions.ImprovedEuler.solve(x0, y0, xn, n, f, e)
        
        runge_kutta    = solutions.RungeKutta.solve(x0, y0, xn, n, f, e)
        
        exact_points = [(x, e(x)) for (x, y) in euler[0]]
        
        self.__canvas_approximation.delete('graph')
        
        self.__canvas_approximation.plot_graph('euler',          euler[0],          **kwargs.get('euler', {}))
        self.__canvas_approximation.plot_graph('improved_euler', improved_euler[0], **kwargs.get('improved_euler', {}))
        self.__canvas_approximation.plot_graph('runge_kutta',    runge_kutta[0],    **kwargs.get('runge_kutta', {}))
        self.__canvas_approximation.plot_graph('exact',          exact_points,      **kwargs.get('exact', {}))
        
        self.__canvas_errors.delete('graph')
        self.__canvas_errors.plot_graph('euler',          euler[1],          **kwargs.get('euler', {}))
        self.__canvas_errors.plot_graph('improved_euler', improved_euler[1], **kwargs.get('improved_euler', {}))
        self.__canvas_errors.plot_graph('runge_kutta',    runge_kutta[1],    **kwargs.get('runge_kutta', {}))
