import os
import sys

import logging
import tkinter
import tkinter.ttk
import tkinter.filedialog
import tkinter.messagebox


class ProjectLoader(tkinter.ttk.Frame):
    """
    A class that load a project with problem.
    
    Attributes
    ----------
          
    label0 : tkinter.ttk.Label
        Display 'Project:'.
    
    label1 : tkinter.ttk.Label
        Display the the of the project.
    
    button : tkinter.ttk.Button
        Open file choosing dialog.
    
    f : callable
        The f function of the problem.
        
    exact : callable
        The exact function of the problem.
        
    defaults : dict
        The default params for the problem.
    
    is_open : bool
        True if the project is loaded.
    
    Methods
    -------
    
    close() : None
        Close current project.
    
    """
    
    def __init__(self, window:tkinter.Misc, last_open_path = '.last', **kwargs):
        """
        Parameters
        ----------
        
        window : tkinter.Misc
            A tkinter parent element.
               
        kwargs : dict
            The same as tkinter.ttk.Frame.
            
        """
        
        super().__init__(window, **kwargs)

        self.f        = None
        self.exact    = None
        self.defaults = None
        
        self.is_open  = False

        self.last_open_path = last_open_path
        
        self.label0 = tkinter.ttk.Label(self,  text = 'Project:'   , padding = 5)
        self.label1 = tkinter.ttk.Label(self,  text = 'No project.', padding = 5)
        self.button = tkinter.ttk.Button(self, text = 'Load', command = self.__load)

        self.label0.grid(row = 0, column = 0)
        self.label1.grid(row = 0, column = 1)
        self.button.grid(row = 0, column = 2, padx = 5)

        self.columnconfigure(1, weight = 1)

        self.bind_all('<<OnStart>>', self.__on_start)

    def close(self):
        """
        Close current project.
        
        """
        
        self.is_open  = False
        
        self.f        = None
        self.exact    = None
        self.defaults = None
        
        self.label1.configure(text = 'No project.')

    def __load(self, path = None) -> None:
        # First start.
        is_on_start = bool(path)
        
        if not is_on_start:
            path = tkinter.filedialog.askopenfilename()
        
        # User close dialog.
        if not path:
            return
        
        dir, filename = os.path.split(path)
        
        filename = os.path.splitext(filename)[0]
        
        sys.path.append(dir)
        
        try:
            project = __import__(filename)
            
        except Exception as exc:
            if not is_on_start:
                tkinter.messagebox.showerror('Load project', f'Cannot open the "{filename}" project: {exc}')
            
            logging.warning('Project openning.', exc_info = True)
            
            return
            
        finally:
            sys.path.pop(-1)
        
        try:
            self.f        = project.f
            self.exact    = project.exact
            self.defaults = getattr(project, 'defaults', {})
            
            self.is_open = True
            
        except Exception as exc:
            self.close()
            
            if not is_on_start:
                tkinter.messagebox.showerror('Load project', f'Cannot load the "{filename}" project: {exc}')
            
            logging.warning('Project loading.', exc_info = True)
            
            return
            
        with open(self.last_open_path, 'w') as file:
            print(path, end = '', file = file)
        
        self.label1.configure(text = filename)
        
        self.event_generate('<<ProjectLoader>>')

    def __on_start(self, event:tkinter.Event) -> None:
        try:
            with open(self.last_open_path) as file:
                path = file.read()
                
        except FileNotFoundError:
            logging.warning('Last project.', exc_info = True)
            
            return
        
        self.__load(path)
