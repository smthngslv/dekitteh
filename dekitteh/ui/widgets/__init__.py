from dekitteh.ui.widgets.axis             import Axis
from dekitteh.ui.widgets.canvas           import Canvas
from dekitteh.ui.widgets.graph_color      import GraphColor
from dekitteh.ui.widgets.project_loader   import ProjectLoader
from dekitteh.ui.widgets.numeric_variable import NumericVariable
