import math
import tkinter
import tkinter.font

from enum import IntEnum

class Axis:
    """
    Represents an axis for the canvas.
    
    Properties
    ----------
    
    scale : float
        Amount of pixels in one point of axis.
    
    Methods
    -------
    
    update() -> None
        Redraw the axis.
        
    configure(**kwargs) -> [dict, None]
        To configure main axis line. Works same as tkinter.Canvas.itemconfigure().
        
    configure_steps(**kwargs) -> [dict, None]
        To configure steps of the axis. Works same as tkinter.Canvas.itemconfigure().
        
    configure_labels(**kwargs) -> [dict, None]
        To configure labels of the axis. Works same as tkinter.Canvas.itemconfigure().
    
    
    """
    
    class Type(IntEnum):
        """
        Represents the type of the axis.
        """
        VERTICAL   = 1
        HORIZONTAL = 0
        
    
    def __init__(self, canvas:tkinter.Canvas, type:Type, **kwargs):
        """
        Parameters
        ----------
        
        canvas : tkinter.Canvas
            A tkinter canvas element.
            
        type : Axis.Type
            Type of the axis.
            
            
        kwargs
        ------
        
        steps : dict
            A kwargs for the steps line of the axis.
            
        labels : dict
            A kwargs for the labels of the axis.
            
            
        Other kwargs will be used to configure main 
        line of the axis via tkinter.Canvas.itemconfigure().
            
        """
        
        self.__canvas = canvas
        self.__type   = Axis.Type(type)
        
        self.__steps_kwargs  = self.__steps_default_kwargs
        self.__labels_kwargs = self.__labels_default_kwargs
        
        self.__labels_size = self.__labels_kwargs['font'].metrics('ascent')
        
        self.__id      = self.__canvas.create_line(0, 0, 0, 0, **self.__default_kwargs)
        self.__unit_id = self.__canvas.create_line(1, 1, 1, 1, state = tkinter.HIDDEN)
        
        self.configure_steps(**kwargs.pop('steps', {}))
        self.configure_labels(**kwargs.pop('labels', {}))
        self.configure(**kwargs)
        
    @property
    def scale(self) -> float:
        """
        Returns the amount of pixels in one point of the axis.
        """
        
        return self.__canvas.coords(self.__unit_id)[self.__type]
    
    def update(self) -> None:
        """
        Redraw the axis.
        """
        
        self.__canvas.delete(self.__steps_kwargs['tags'])
        self.__canvas.delete(self.__labels_kwargs['tags'])
        
        scale          = self.scale
        step, accuracy = self.__step_and_accuracy
        
        labels_background_kwargs = {'outline': self.__canvas.cget('background'), 
                                    'fill':    self.__canvas.cget('background'),
                                    'tags':    self.__labels_kwargs['tags']}
        
        xf, xt = self.__canvas.canvasx(0), self.__canvas.canvasx(self.__canvas.winfo_width())
        yf, yt = self.__canvas.canvasy(0), self.__canvas.canvasy(self.__canvas.winfo_height())
        
        if self.__type == Axis.Type.HORIZONTAL:
            start = xf
            end   = xt
                 
            subaxis_start = yf
            subaxis_end   = yt
            
            self.__canvas.coords(self.__id, start, 0, end, 0)
            
            current = start // step * step
            
            while current < end:
                if -step * 1.5 < current < step * 0.5:
                    current += step; continue
                
                format = 'f' if abs(current / scale) < 10**6 else 'e'
                
                text = f' %.{accuracy}{format}' % (current / scale)
                
                # subaxis
                self.__canvas.create_line(current,  0, current, subaxis_start, **self.__steps_kwargs)
                self.__canvas.create_line(current,  0, current, subaxis_end,   **self.__steps_kwargs)
                
                #self.__canvas.create_line(current, -5, current, 5, **self.__steps_kwargs)
                label      = self.__canvas.create_text(current, 10, text = text, **self.__labels_kwargs)
                background = self.__canvas.create_rectangle(self.__canvas.bbox(label), **labels_background_kwargs)
                
                self.__canvas.tag_lower(background, label)
  
                current += step
            
                
        else:
            start = yf
            end   = yt
            
            subaxis_start = xf
            subaxis_end   = xt
            
            self.__canvas.coords(self.__id, 0, start, 0, end)
            
            current = start // step * step
            
            while current < end:
                if -step * 0.5 < current < step * 1.5:
                    current += step; continue
                
                format = 'f' if abs(current / scale) < 10**6 else 'e'
                
                text = f' %.{accuracy}{format}' % (-current / scale)
                
                # subaxis
                self.__canvas.create_line(0, current, subaxis_start, current, **self.__steps_kwargs)
                self.__canvas.create_line(0, current, subaxis_end,   current, **self.__steps_kwargs)
                
                #self.__canvas.create_line(-5, current, 5, current, **self.__steps_kwargs)
                label      = self.__canvas.create_text(-10, current, text = text, **self.__labels_kwargs)
                background = self.__canvas.create_rectangle(self.__canvas.bbox(label), **labels_background_kwargs)
                
                self.__canvas.tag_lower(background, label)
                
                current += step
    
    def configure(self, **kwargs) -> [dict, None]:
        """
        To configure main axis line. Works same as tkinter.Canvas.itemconfigure().
        """
        
        return self.__canvas.itemconfigure(self.__id, **kwargs)
    
    def configure_steps(self, **kwargs) -> [dict, None]:
        """
        To configure steps of the axis. Works same as tkinter.Canvas.itemconfigure().
        """
        
        if kwargs:
            self.__steps_kwargs.update(kwargs)
            
        else:
            return self.__steps_kwargs.copy()
            
    def configure_labels(self, **kwargs) -> [dict, None]:
        """
        To configure labels of the axis. Works same as tkinter.Canvas.itemconfigure().
        """
        
        if kwargs:
            self.__labels_kwargs.update(kwargs)
            
            self.__labels_size = self.__labels_kwargs['font'].metrics('ascent')
            
        else:
            return self.__labels_kwargs.copy()
    
    @property
    def __step_and_accuracy(self) -> tuple[float, int]:
        steps = [(0.5, 1), (0.2, 1), (0.1, 1)]
        
        scale = self.scale
        exact = 5 *  self.__labels_size / scale
        
        if exact > 5:
            return (scale * (int(exact) // 5 * 5), 0)
        
        if exact > 1:
            return (scale * int(exact), 0)
        
        p = - min(int(math.log(exact, steps[-1][0])), 2)
        
        for (step, accuracy) in steps:
            if exact > step * 10 ** p:
                return (scale * step * 10 ** p, accuracy - p)
        
        return (scale * steps[-1][0] * 10 ** p, steps[-1][1] - p)

    @property
    def __default_kwargs(self) -> dict:
        default_kwargs = {}
        
        if self.__type == Axis.Type.HORIZONTAL:
            default_kwargs['arrow'] =  tkinter.LAST
            
        else:
            default_kwargs['arrow'] =  tkinter.FIRST
            
        return default_kwargs
    
    @property
    def __steps_default_kwargs(self) -> dict:
        default_kwargs = {'dash':(2, 5)}
        
        if self.__type == Axis.Type.HORIZONTAL:
            default_kwargs['tags']   = 'horizontal_steps'
            
        else:
            default_kwargs['tags']   = 'vertical_steps'
            
        return default_kwargs
    
    @property
    def __labels_default_kwargs(self) -> dict:
        default_kwargs = {'font': tkinter.font.Font(size = 10)}
        
        if self.__type == Axis.Type.HORIZONTAL:
            default_kwargs['tags']   = 'horizontal_labels'
            default_kwargs['angle']  = -45
            default_kwargs['anchor'] = tkinter.NW
            
        else:
            default_kwargs['tags']   = 'vertical_labels'
            default_kwargs['anchor'] = tkinter.E
            
        return default_kwargs
