import logging
import tkinter
import tkinter.ttk

# 
class NumericVariable(tkinter.ttk.Frame):
    """
    A class that represent a numeric value 
    by creating an entry and scale tkinter widgets.
    
    Attributes
    ----------
         
    accuracy : int
        Default is 1, the number of numbers after point.
            
    scale_step : float
        Default is 0.1, the step for scale widget.
         
    entry : tkinter.ttk.Entry
        The entry widget.
    
    scale : tkinter.ttk.Scale
        The scale widget.
    
    left_label : tkinter.ttk.Label
        Display the left border of the possible scale.
    
    right_label : tkinter.ttk.Label
        Display the right border of the possible scale.
    
    current_label : tkinter.ttk.Label
        Display the name of the variable.
    
    
    Properties
    ----------
    
    value : float
        The value of the variable.
    
    """
    def __init__(self, window:tkinter.Misc, name:str, **kwargs):
        """
        Parameters
        ----------
        
        window : tkinter.Misc
            A tkinter parent element.
            
        name : str
            The variable name.
        
        
        kwargs
        ------
         
        accuracy : int
            Default is 1, the number of numbers after point.
            
        scale_step : float
            Default is 0.1, the step for scale widget.
            
        initial_value : float
            Initial value of the widget.
        
        from_ : float
            Default is 0.5, the left border of the scale widget.
        
        to : float
            Default is 2.0, the right border of the scale widget.
        
        Other kwargs are the same as for tkinter.ttk.Frame.
        
        """
        
        self.accuracy   = kwargs.pop('accuracy',     1)
        self.scale_step = kwargs.pop('scale_step', 0.1)
        
        # The value without tuning.
        self.__raw_value   = round(float(kwargs.pop('initial_value', 1)), self.accuracy)
        
        self.__entry_value = tkinter.StringVar(value = f'%.{self.accuracy}f' % self.__raw_value)
        
        scale_kwargs = {
            'from_':   kwargs.pop('from_', 0.5),
            'to':      kwargs.pop('to',    2.0),
            'value':   1.0,
            'command': self.__update_tuning
        }
        
        super().__init__(window, **kwargs)
        
        entry_kwargs = {
            'textvariable':    self.__entry_value,
            'validate':        'key',
            'validatecommand': (self.register(self.__update_exact), '%P')
        }
        
        self.entry = tkinter.ttk.Entry(self, **entry_kwargs)
        self.scale = tkinter.ttk.Scale(self, **scale_kwargs)
        
        self.left_label    = tkinter.ttk.Label(self, text = '%d%%' % (scale_kwargs['from_'] * 100), padding = 5)
        self.right_label   = tkinter.ttk.Label(self, text = '%d%%' % (scale_kwargs['to']    * 100), padding = 5)
        self.current_label = tkinter.ttk.Label(self, text = name,                                   padding = 5)
        
        
        self.entry.grid(row = 0, column = 1, sticky = tkinter.E + tkinter.W)      
        self.scale.grid(row = 1, column = 1, sticky = tkinter.E + tkinter.W)
        
        self.left_label.grid(row = 1, column = 0)
        self.right_label.grid(row = 1, column = 2)
        self.current_label.grid(row = 0, column = 0)
        
        self.columnconfigure(1, weight = 1)
        
    @property
    def value(self) -> float:
        """
        Returns the value of the variable.
        """
        
        value = self.__entry_value.get()
        
        if value:
            return float(value)
        
        return self.__raw_value
    
    @value.setter
    def value(self, value:float) -> None:
        """
        Set the value and reset the scale widget.
        """
        
        self.scale.set(1.0)
        self.__raw_value = float(value)
        self.__entry_value.set(value)
    
    def __update_exact(self, value:str) -> bool:
        try:
            self.__raw_value = float(value)
            
        except ValueError:
            # The empty string is valid.
            return not value
        
        self.scale.set(1.0)
        
        self.event_generate('<<NumericVariable>>')

        return True
    
    def __update_tuning(self, value:float) -> None:
        # Round to step.
        value = (float(value) // self.scale_step + 1) * self.scale_step
        
        # Compute value.
        value = round(self.__raw_value * value, self.accuracy)
        
        # If any changes.
        if self.value != value:
            self.__entry_value.set(f'%.{self.accuracy}f' %  value)
            
            self.event_generate('<<NumericVariable>>')