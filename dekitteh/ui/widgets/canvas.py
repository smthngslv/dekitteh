import sys
import time
import math
import tkinter
import tkinter.font

from dekitteh.ui.widgets import Axis

class Canvas(tkinter.Canvas):
    """
    Add possibility to drow axis and graphs to standart tkinter.Canvas.
    
    Attributes
    ----------
    
    ox : Axis
        The OX axis.
    
    oy : Axis
        The OY axis.
    
    min_update_delay : float
        A minimum delay between canvas updates.
    
    scaling_sensitivity : float
            Delta for the scaling.
    
    Methods
    -------
    
    plot_graph(name:str, points:list[tuple[float, float]], adaptive = False, **kwargs) -> int
        Plot the graph with the given name. If adaptive true - selects the appropriate scale.
        The kwargs use to configure the canvas line, same for tkinter.Canvas.create_line.
    
    """
    
    def __init__(self, window:tkinter.Misc, **kwargs):
        """
        Parameters
        ----------
        
        window : tkinter.Misc
            A tkinter parent element.
            
            
        kwargs
        ------
        
        ox : dict
            A kwargs for the Axis class, OX.
            
        oy : dict
            A kwargs for the Axis class, OY.
        
        update_delay : float
            Default is 0.016 (60fps). A minimum delay between canvas updates.
            
        scaling_sensitivity: float
            Default is 0.05. Delta for the scaling.
        
        Other kwargs will be used to configure canvas via tkinter.Canvas.
        
        """
        
        ox_kwargs = kwargs.pop('ox', {})
        oy_kwargs = kwargs.pop('oy', {})
        
        self.min_update_delay    = kwargs.pop('update_delay',       0.016)
        self.scaling_sensitivity = kwargs.pop('scaling_sensitivity', 0.05)
        
        kwargs['xscrollincrement'] = 1 
        kwargs['yscrollincrement'] = 1
        
        super().__init__(window, **kwargs)
        
        self.ox = Axis(self, Axis.Type.HORIZONTAL, **ox_kwargs)
        self.oy = Axis(self, Axis.Type.VERTICAL,   **oy_kwargs)
        
        self.__last_size         = None
        self.__last_update       = 0
        self.__mouse_position    = (0, 0)
        self.__is_mouse_pressed  = False
        self.__is_contol_pressed = False
        
        self.bind('<Motion>',          self.__on_moving)
        self.bind('<ButtonPress-1>',   self.__on_press_mouse)
        self.bind('<ButtonRelease-1>', self.__on_release_mouse)
        
        self.bind('<Configure>', self.__on_resize)
        
        self.bind_all("<KeyPress-Control_L> ",  self.__on_press_control,   add = '+')
        self.bind_all("<KeyRelease-Control_L>", self.__on_release_control, add = '+')
        
        if sys.platform == 'linux' or sys.platform == 'darwin':
            self.bind('<Button-4>', lambda event:self.__on_scale(event, False, event.state))
            self.bind('<Button-5>', lambda event:self.__on_scale(event,  True, event.state))
        
        else:
            self.bind("<MouseWheel>", lambda event:self.__on_scale(event, event.delta > 0, self.__is_contol_pressed))
            
    def plot_graph(self, name:str, points:list[tuple[float, float]], adaptive = False, **kwargs) -> int:
        """
        Plot the graph with the given name. If adaptive true - selects the appropriate scale.
        The kwargs use to configure the canvas line, same for tkinter.Canvas.create_line.
        
        Parameters
        ----------
        
        name : str
            The name of the graph, used as a tag.
            
        points : list[tuple[float, float]]
            The array of points.
            
        adaptive : bool
            Default is False. If adaptive true - selects the appropriate scale.
        
        The kwargs use to configure the canvas line, same for tkinter.Canvas.create_line.
        
        """
        
        squeezed_points = []
        
        scale_x = self.ox.scale
        scale_y = self.oy.scale
        
        if adaptive:
            # X we already know.
            x_min, x_max = min(points[0][0], 0), max(points[-1][0], 0)
            # Y need to be computed.
            y_min, y_max = points[0][1], points[0][1]
        
        for point in points:
            squeezed_points.append( point[0] * scale_x)
            
            if adaptive:
                y_min = min(point[1], y_min)
                y_max = max(point[1], y_max)
            
            squeezed_points.append(-point[1] * scale_y)
       
        id = self.create_line(*squeezed_points, tags = ('graph', name), **kwargs)
        
        if adaptive:
            y_min, y_max = min(y_min, 0), max(y_max, 0)
        
            scale_x_new = self.winfo_width()  / abs(x_max - x_min) / 3 
            scale_y_new = self.winfo_height() / abs(y_max - y_min) / 3
            
            self.scale('all', 0, 0, scale_x_new / scale_x, scale_y_new / scale_y)
            
            self.__update()
         
        return id
        
    def __update(self) -> None:
        self.ox.update()
        self.oy.update()
        
        self.__last_update = time.time()
        
    def __on_scale(self, event:tkinter.Event, is_decrease = False, is_horizontal = False) -> None:
        if time.time() - self.__last_update < self.min_update_delay:
            return
        
        x = self.canvasx(event.x)
        y = self.canvasy(event.y)
        
        if is_decrease:
            delta = 1 - self.scaling_sensitivity
        else:
            delta = 1 + self.scaling_sensitivity
        
        scale = (delta, 1) if is_horizontal else (1, delta)
        
        self.scale('all', 0, 0, *scale)
        
        self.xview(tkinter.SCROLL, int(x * (scale[0] - 1)), tkinter.UNITS)
        self.yview(tkinter.SCROLL, int(y * (scale[1] - 1)), tkinter.UNITS)

        self.__update()

    def __on_resize(self, event:tkinter.Event) -> None:        
        if self.__last_size:
            scale_x = event.width  / self.__last_size[0]
            scale_y = event.height / self.__last_size[1]
            
            self.scale('all', 0, 0, scale_x, scale_y)
            
            self.xview(tkinter.SCROLL, int(self.canvasx(0) * (scale_x - 1)), tkinter.UNITS)
            self.yview(tkinter.SCROLL, int(self.canvasy(0) * (scale_y - 1)), tkinter.UNITS)
        
        # First update.
        else:
            # Set (0, 0) point to the center.
            self.xview(tkinter.SCROLL, - event.width  // 2, tkinter.UNITS)
            self.yview(tkinter.SCROLL, - event.height // 2, tkinter.UNITS)
            
        self.__last_size = (event.width, event.height)
        
        self.__update()
        
    def __on_moving(self, event:tkinter.Event) -> None:
        if time.time() - self.__last_update < self.min_update_delay:
            return
        
        if self.__is_mouse_pressed:
            delta_x = self.__mouse_position[0] - event.x
            delta_y = self.__mouse_position[1] - event.y
            
            self.xview(tkinter.SCROLL, delta_x, tkinter.UNITS)
            self.yview(tkinter.SCROLL, delta_y, tkinter.UNITS)
            
            self.__update()
        
        self.__mouse_position = (event.x, event.y)
    
    def __on_press_mouse(self, event:tkinter.Event) -> None:
        self.__is_mouse_pressed = True
    
    def __on_release_mouse(self, event:tkinter.Event) -> None:
        self.__is_mouse_pressed = False

    def __on_press_control(self, event:tkinter.Event) -> None:
        self.__is_contol_pressed = True

    def __on_release_control(self, event:tkinter.Event) -> None:
        self.__is_contol_pressed = False

