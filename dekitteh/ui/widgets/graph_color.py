import logging
import tkinter
import tkinter.ttk
import tkinter.colorchooser

class GraphColor(tkinter.ttk.Frame):
    """
    A class that represent a settings of graph color.
    
    Attributes
    ----------
         
    label0 : tkinter.ttk.Label
        Display the name of the graph.
    
    label1 : tkinter.ttk.Label
        Display the color.
    
    check_button : tkinter.ttk.Checkbutton
        Controls visiability of the graph.
    
    button : tkinter.ttk.Button
        Open choosing color dialog.
    
    
    Properties
    ----------
    
    value : str
        The current color.
        
    is_enabled : bool
        Returns True if this graph enabled.
    
    """
    def __init__(self, window:tkinter.Misc, name:str, initial_color = '#000000', **kwargs):
        """
        Parameters
        ----------
        
        window : tkinter.Misc
            A tkinter parent element.
            
        name : str
            The graph name.
         
        initial_color : str
            Initial color, default is #000000
        
        kwargs : dict
            The same as tkinter.ttk.Frame.
        
        """
        
        super().__init__(window, **kwargs)
        
        self.check_button = tkinter.ttk.Checkbutton(self, command = lambda:self.event_generate('<<GraphColor>>'))
        
        self.check_button.invoke()
        
        self.label0 = tkinter.ttk.Label(self,  text = name, padding = 5)
        self.label1 = tkinter.ttk.Label(self,  background = initial_color, padding = 5, width = 2)
        self.button = tkinter.ttk.Button(self, text = 'Change', command = self.__on_change)
        
        self.check_button.grid(row = 0, column = 0, padx = 5)

        self.label0.grid(row = 0, column = 1)
        self.label1.grid(row = 0, column = 2, sticky = tkinter.E)
        self.button.grid(row = 0, column = 3, padx = 5)

        self.columnconfigure(1, weight = 1)
        
    @property
    def value(self) -> str:
        """
        Returns the current color.
        """
        
        return str(self.label1.cget('background'))
    
    @value.setter
    def value(self, value) -> None:
        """
        Sets the current color.
        """
        
        self.label1.configure(background = value)
    
    @property 
    def is_enabled(self) -> bool:
        """
        Returns True if this graph enabled.
        """
        
        return ('selected' in self.check_button.state())
     
    def __on_change(self) -> None:
        result = tkinter.colorchooser.askcolor(self.value)[1]
        
        if not result:
            return
        
        self.label1.configure(background = result)
        
        self.event_generate('<<GraphColor>>')
