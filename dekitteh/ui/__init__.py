from dekitteh.ui.errors_frame        import ErrorsFrame
from dekitteh.ui.controls_frame      import ControlsFrame 
from dekitteh.ui.approximation_frame import ApproximationFrame
