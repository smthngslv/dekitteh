from dekitteh.solutions.solution import Solution

class RungeKutta(Solution):
    @staticmethod
    def compute(x:float, y:float, h:float, f:callable) -> float:
        k1i = f(x, y)
        k2i = f(x + h/2, y + h/2 * k1i)
        k3i = f(x + h/2, y + h/2 * k2i)
        k4i = f(x + h,   y + h   * k3i)
        
        return (y + h/6 * (k1i + 2 * k2i + 2 * k3i + k4i))
 
