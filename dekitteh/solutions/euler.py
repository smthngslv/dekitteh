from dekitteh.solutions.solution import Solution

class Euler(Solution):
    @staticmethod
    def compute(x:float, y:float, h:float, f:callable) -> float:
        return (y + h * f(x, y)) 
