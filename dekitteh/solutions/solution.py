from abc import ABC, abstractmethod

class Solution(ABC):
    @staticmethod
    @abstractmethod
    def compute(x:float, y:float, h:float, f:callable) -> float:
        raise NotImplementedError()
    
    @classmethod
    def solve(cls, x0:float, y0:float, xn:float, n:int, f:callable, exact:callable) -> tuple[list, list]:
        h = (xn - x0) / n
        
        GTE = [(x0, 0), ]

        points = [(x0, y0)]
         
        for i in range(n):
            xi, yi = points[i]
            
            # i + 1
            x = xi + h
            y = cls.compute(xi, yi, h, f)
            
            points.append((x, y))
            
            # i + 1
            GTE.append((x, abs(exact(x) - y)))

        return (points, GTE)

