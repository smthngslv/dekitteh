from dekitteh.solutions.solution import Solution

class ImprovedEuler(Solution):
    @staticmethod
    def compute(x:float, y:float, h:float, f:callable) -> float:
        return (y + (h / 2) * (f(x, y) + f(x + h, y + h * f(x, y)))) 
 
