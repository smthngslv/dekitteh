import tkinter
import tkinter.ttk

from dekitteh.ui import ErrorsFrame, ApproximationFrame, ControlsFrame 

class App(tkinter.Tk):
    """
    Main app class.
    
    """
    
    def __init__(self, **kwargs):
        """
        Kwargs are the same as tkinter.Tk.
        """
        
        super().__init__(**kwargs)
        
        style = tkinter.ttk.Style()
        
        if 'clam' in style.theme_names():
            style.theme_use('clam')
        
        self.title('DeKitteh Approximation App')
        
        self.minsize(500, 500) 
        
        self.__tabs = tkinter.ttk.Notebook(self)
        
        self.__tabs.bind('<<NotebookTabChanged>>', lambda event: self.__update(False))
        
        self.__aproximate_frame = ApproximationFrame(self.__tabs)
        self.__errors_frame     = ErrorsFrame(self.__tabs)
        
        tab1 = self.__tabs.add(self.__aproximate_frame, text = 'Solutions & GTE')
        tab2 = self.__tabs.add(self.__errors_frame,     text = 'GTE of N')
        
        self.__contols_frame = ControlsFrame(self)
        
        self.bind('<<ProjectLoader>>',   lambda event:self.__update())
        self.bind('<<GraphColor>>',      lambda event:self.__update(False))
        self.bind('<<NumericVariable>>', lambda event:self.__update())
        
        self.__tabs.grid(row = 0, column = 0, sticky = tkinter.N + tkinter.S + tkinter.W + tkinter.E)
        self.__contols_frame.grid(row = 0, column = 1, sticky = tkinter.N + tkinter.S)
        
        self.rowconfigure(0,    weight = 1)
        self.columnconfigure(0, weight = 1)
        
        self.after(100, lambda: self.__tabs.select(self.__tabs.tabs()[1]))
        self.after(200, lambda: self.__tabs.select(self.__tabs.tabs()[0]))
        self.after(300, lambda: self.event_generate('<<OnStart>>'))
        
    def __update(self, adaptive = True) -> None:
        if not self.__contols_frame.project.is_open:
            return
        
        x0 = self.__contols_frame.x0.value
        y0 = self.__contols_frame.y0.value
        xn = self.__contols_frame.xn.value
        n  = int(self.__contols_frame.n.value)
        n0 = int(self.__contols_frame.n0.value)
        N  = int(self.__contols_frame.N.value)
        
        f = self.__contols_frame.project.f
        e = self.__contols_frame.project.exact
        
        kwargs = {'euler': 
                      {'fill': self.__contols_frame.euler.value,
                       'width': 2,
                       'adaptive': adaptive,
                       'state': tkinter.NORMAL if self.__contols_frame.euler.is_enabled else tkinter.HIDDEN }, 
                      
                  'improved_euler': 
                      {'fill': self.__contols_frame.improved_euler.value, 
                       'width': 2,
                       'state': tkinter.NORMAL if self.__contols_frame.improved_euler.is_enabled else tkinter.HIDDEN },
                      
                  'runge_kutta': 
                      {'fill': self.__contols_frame.runge_kutta.value, 
                       'width': 2, 
                       'state': tkinter.NORMAL if self.__contols_frame.runge_kutta.is_enabled else tkinter.HIDDEN },
                      
                  'exact':
                      {'fill': self.__contols_frame.exact.value,
                       'width': 2, 
                       'adaptive': adaptive,
                       'state': tkinter.NORMAL if self.__contols_frame.exact.is_enabled else tkinter.HIDDEN }}
        
        if adaptive:
            self.__aproximate_frame.rebuild(f, e, x0, y0, xn, n, **kwargs)
            self.__errors_frame.rebuild(f, e, x0, y0, xn, n0, N, **kwargs)
        
        elif self.__tabs.index('current') == 0:
            self.__aproximate_frame.rebuild(f, e, x0, y0, xn, n, **kwargs)
            
        else:
            self.__errors_frame.rebuild(f, e, x0, y0, xn, n0, N, **kwargs)
